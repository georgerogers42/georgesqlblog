using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NodaTime;
using NodaTime.Extensions;
using GeorgeSQLBlog.Models;

namespace GeorgeSQLBlog.Controllers {
    public class HomeController : Controller {
        [HttpGet]
        [Route("")]
        public IActionResult Index() {
            using(var ctx = new Context()) {
                var user = ctx.Login(HttpContext.Session.GetString("email"));
                ViewData["user"] = user;
                var users = from u in ctx.Users orderby u.Id select u;
                ViewData["users"] = users.ToList();
                return View();
            }
        }

        [HttpGet]         
        [Route("user/{Id}")]
        public IActionResult ViewUser(int id) {
            using(var ctx = new Context()) {
                var user = ctx.Login(HttpContext.Session.GetString("email"));
                var users = (from u in ctx.Users where u.Id == id select u).ToList();
                foreach(var u in users) {
                    u.Articles = (from a in ctx.Articles.Include(a => a.User) where a.User.Id == u.Id orderby a.PostedOn descending select a).ToList();
                }
                ViewBag.User = user;
                ViewBag.Users = users;
                return View();
            }
        }

        [HttpGet]
        [Route("article/{Slug}")]
        public IActionResult ViewArticle(string slug) {
            using(var ctx = new Context()) {
                var user = ctx.Login(HttpContext.Session.GetString("email"));
                var articles = from a in ctx.Articles.Include(a => a.User) where a.Slug == slug select a;
                ViewBag.User = user;
                ViewBag.Articles = articles.ToList();
                return View();
            }
        }
        [HttpPost]
        [Route("article/{Slug}")]
        public IActionResult UpdateArticle(string slug) {
            using(var ctx = new Context()) {
                var user = ctx.Login(HttpContext.Session.GetString("email"));
                if(user == null) return Redirect("/");
                var articles = (from a in ctx.Articles.Include(a => a.User) where a.Slug == slug where a.User.Id == user.Id select a).ToList();
                if(articles.Count > 0) {
                    var a = articles[0];
                    a.UpdatedOn = new ZonedDateTime(SystemClock.Instance.GetCurrentInstant(), DateTimeZone.Utc);
                    a.Title = Request.Form["title"];
                    a.Contents = Request.Form["contents"];
                    ctx.Articles.Update(a);
                    ctx.SaveChanges();
                    return Redirect(string.Format("/article/{0}", a.Slug));
                } else {
                    return Redirect("/");
                }
            }
        }
        [HttpPost]
        [Route("article/{Slug}/delete")]
        public IActionResult DeleteArticle(string slug) {
            using(var ctx = new Context()) {
                var user = ctx.Login(HttpContext.Session.GetString("email"));
                if(user == null) return Redirect("/");
                ctx.Articles.RemoveRange((from a in ctx.Articles.Include(a => a.User) where a.User.Id == user.Id where a.Slug == slug select a).ToList());
                ctx.SaveChanges();
                return Redirect(string.Format("/user/{0}", user.Id));
            }
        }

        [HttpPost]
        [Route("articles/create")]
        public IActionResult CreatePost() {
            using(var ctx = new Context()) {
                var user = ctx.Login(HttpContext.Session.GetString("email"));
                var article = new Article();
                article.User = user;
                article.Title = Request.Form["title"];
                article.Slug = Request.Form["slug"];
                article.Contents = Request.Form["contents"];
                ctx.Articles.Add(article);
                ctx.SaveChanges();
                return Redirect(string.Format("/article/{0}", article.Slug));
            }
        }
        
        [HttpPost]
        [Route("signup")]
        public IActionResult CreateUser() {
            using(var ctx = new Context()) {
                var user = new User();
                user.Name = Request.Form["name"];
                user.Email = Request.Form["email"];
                user.Clearpass = Request.Form["clearpass"];
                ctx.Users.Add(user);
                ctx.SaveChanges();
                return Redirect("/");
            }
        }

        [HttpPost]
        [Route("login")]
        public IActionResult Login() {
            using(var ctx = new Context()) {
                var user = ctx.Login(Request.Form["email"], Request.Form["clearpass"]);
                if(user != null) {
                    HttpContext.Session.SetString("email", user.Email);
                }
                return Redirect(string.Format("/user/{0}", user.Id));
            }
        }
        [HttpPost]
        [Route("logout")]
        public IActionResult Logout() {
            HttpContext.Session.Clear();
            return Redirect("/");
        }

        [Route("error")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error() {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
