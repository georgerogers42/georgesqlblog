﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Npgsql.EntityFrameworkCore.PostgreSQL.NodaTime;

namespace GeorgeSQLBlog.Models {
    public class Context : DbContext {
        public Context() { }

        public Context(DbContextOptions<Context> options) : base(options) { }

        public virtual DbSet<Article> Articles { get; set; }
        public virtual DbSet<User> Users { get; set; }

        public User Login(string email) {
            var users = (from u in Users where u.Email == email select u).ToList();
            return users.Count > 0 ? users[0] : null;
        }
        public User Login(string email, string clearpass) {
            var user = Login(email);
            return (user != null && user.HasClearpass(clearpass)) ? user : null;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            var connString = Environment.GetEnvironmentVariable("DB_CONN");
            connString = connString != null ? connString : "Host=localhost;Database=blog";
            optionsBuilder.UseNpgsql(connString, o => {
                    o.UseNodaTime();
                    });
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<Article>(entity => {
                    entity.Property(p => p.Contents).HasColumnType("text");
                    entity.HasIndex(e => e.Slug)
                    .HasName("AK_Article_Slug")
                    .IsUnique();
                    }).Entity<Article>().HasOne(a => a.User).WithMany(a => a.Articles);
            modelBuilder.Entity<User>(entity => {
                    entity.HasIndex(e => e.Email)
                    .HasName("AK_Users_Email")
                    .IsUnique();
                    entity.Property(e => e.Email).IsRequired();
                    });
        }
    }
}
