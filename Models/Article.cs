using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using NodaTime;
using NodaTime.Text;
using NodaTime.Extensions;

namespace GeorgeSQLBlog.Models {
    [DataContract]
    public class Article {
        public Article() {
            UpdatedOn = new ZonedDateTime(SystemClock.Instance.GetCurrentInstant(), DateTimeZone.Utc);
            PostedOn = new ZonedDateTime(SystemClock.Instance.GetCurrentInstant(), DateTimeZone.Utc);
        }
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public User User { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Slug { get; set; }
        [DataMember]
        public string Contents { get; set; }
        [DataMember]
        public ZonedDateTime PostedOn { get; set; }
        [DataMember]
        public ZonedDateTime UpdatedOn { get; set; }
        public string Updated() {
            return ZonedDateTimePattern.GeneralFormatOnlyIso.Format(UpdatedOn);
        }
        public void Updated(string v) {
            UpdatedOn = new ZonedDateTime(DateTime.Parse(v).ToInstant(), DateTimeZone.Utc);
        }
        public string Posted() {
            return ZonedDateTimePattern.GeneralFormatOnlyIso.Format(PostedOn);            
        }
        public void Posted(string v) {
            PostedOn = new ZonedDateTime(DateTime.Parse(v).ToInstant(), DateTimeZone.Utc);
        }
    }
}
