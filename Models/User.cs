using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Isopoh.Cryptography.Argon2;

namespace GeorgeSQLBlog.Models {
    [DataContract]
    public class User {
        public User() { }
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Email { get; set; }
        public string LongEmail {
            get {
                return string.Format("{0} <{1}>", Name, Email);
            }
        }
        [DataMember]
        public string Password { get; set; }


        [DataMember]
        public List<Article> Articles { get; set; }
        public bool HasClearpass(string clearpass) {
            return Argon2.Verify(Password, clearpass);
        }
        public string Clearpass {
            set {
                Password = Argon2.Hash(value);
            }
        }
    }
}
