﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NodaTime;

namespace GeorgeSQLBlog.Migrations
{
    public partial class updated_on : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "PostedOn",
                table: "Articles",
                nullable: false,
                oldClrType: typeof(ZonedDateTime));

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedOn",
                table: "Articles",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UpdatedOn",
                table: "Articles");

            migrationBuilder.AlterColumn<ZonedDateTime>(
                name: "PostedOn",
                table: "Articles",
                nullable: false,
                oldClrType: typeof(DateTime));
        }
    }
}
