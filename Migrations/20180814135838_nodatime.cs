﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NodaTime;

namespace GeorgeSQLBlog.Migrations
{
    public partial class nodatime : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<Instant>(
                name: "UpdatedOn",
                table: "Articles",
                nullable: false,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<Instant>(
                name: "PostedOn",
                table: "Articles",
                nullable: false,
                oldClrType: typeof(DateTime));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedOn",
                table: "Articles",
                nullable: false,
                oldClrType: typeof(Instant));

            migrationBuilder.AlterColumn<DateTime>(
                name: "PostedOn",
                table: "Articles",
                nullable: false,
                oldClrType: typeof(Instant));
        }
    }
}
