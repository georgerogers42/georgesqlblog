﻿using Microsoft.EntityFrameworkCore.Migrations;
using NodaTime;

namespace GeorgeSQLBlog.Migrations
{
    public partial class zoned_time2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<ZonedDateTime>(
                name: "UpdatedOn",
                table: "Articles",
                nullable: false,
                oldClrType: typeof(Instant));

            migrationBuilder.AlterColumn<ZonedDateTime>(
                name: "PostedOn",
                table: "Articles",
                nullable: false,
                oldClrType: typeof(Instant));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<Instant>(
                name: "UpdatedOn",
                table: "Articles",
                nullable: false,
                oldClrType: typeof(ZonedDateTime));

            migrationBuilder.AlterColumn<Instant>(
                name: "PostedOn",
                table: "Articles",
                nullable: false,
                oldClrType: typeof(ZonedDateTime));
        }
    }
}
