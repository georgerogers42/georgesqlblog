﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NodaTime;

namespace GeorgeSQLBlog.Migrations
{
    public partial class zoned_time : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<ZonedDateTime>(
                name: "PostedOn",
                table: "Articles",
                nullable: false,
                oldClrType: typeof(DateTime));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "PostedOn",
                table: "Articles",
                nullable: false,
                oldClrType: typeof(ZonedDateTime));
        }
    }
}
